import { Injectable } from '@nestjs/common';
import { StatusRequestDto } from './dto/status.dto';

@Injectable()
export class AppService {
  getRoot(): string {
    return '';
  }

  getStatus(): StatusRequestDto {
    return { status: 'OK' }
  };
}

