import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { StatusRequestDto } from './dto/status.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getRoot(): string {
    return this.appService.getRoot();
  }

  @Get('/status')
  getStatus(): StatusRequestDto {
    return this.appService.getStatus();
  }
}
