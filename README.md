# Welcome to DevOps test

This test intent to check some kubernetes basics and cloud architecture skills.
You will find in this repository a dockerfile and an NodeJS api.

The goal of this test is to propose a Kubernetes manifests and a sample aws architecture.

## Kubernetes cluster details

The cluster is configured with:
- EKS
- 1.14
- Nginx Ingress controller
- Cert-Manager

## Part 1: Kubernetes manifests

The goal in this part is to provide a kubernetes manifest to deploy the NodeJS API
To simplify the test, we have already built and pushed the docker image on a public registry available here: `aegirops/sample-api:1.0.0`.

- Manifest files should be pushed in `k8s` folder.
- Manifest files should implement an ingress object (nginx type) with cert-manager annotations (feel free to use a dummy domain and dummy annotations as we don't have a real world cluster to test it).

## Part 2: AWS Architecture

The part two is a pure architecture design exercise.
The goal here is to propose a simple EKS based architecture to deploy and scale the previous NodeJS API.

Please consider the following requirements:

- NodeJS Api should be able to scale horizontally
- PostgreSQL database required
- Redis required 

The output of this part can be an architecture schema and a quick explanation paragraph.

## How to deliver

You can fork/clone this repository and send us back your own repository link with the updated content.
If you want to make it private, please add jeremy.marjollet@troops.fr on the repo.

## Questions ?

Please address all questions to jeremy.marjollet@troops.fr or via LinkedIn
